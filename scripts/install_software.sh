#!/usr/bin/enc bash
set -euo pipefail

#virtualenv --python=python2.7 py2.7_env

pip install snakemake pyyaml biopython numpy pandas biosevices



mkdir -p src/
pushd src/



# blast
wget -c ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.3.0/ncbi-blast-2.3.0+-x64-linux.tar.gz
tar xvf ncbi-blast-2.3.0+-x64-linux.tar.gz
cp ncbi-blast-2.3.0+/bin/* ../bin/



# InterproScan
wget -c ftp://ftp.ebi.ac.uk/pub/software/unix/iprscan/5/5.17-56.0/interproscan-5.17-56.0-64-bit.tar.gz
tar -pxvzf interproscan-5.17-56.0-64-bit.tar.gz
pushd interproscan-5.17-56.0/data/
wget -c ftp://ftp.ebi.ac.uk/pub/software/unix/iprscan/5/data/panther-data-10.0.tar.gz
tar pxvzf panther-data-10.0.tar.gz
popd



# BUSCO
wget -c http://busco.ezlab.org/files/BUSCO_v1.1b1.tar.gz
tar xvfp BUSCO_v1.1b1.tar.gz



# Hmmer3
wget -c http://eddylab.org/software/hmmer3/3.1b2/hmmer-3.1b2-linux-intel-x86_64.tar.gz
tar xvfp hmmer-3.1b2-linux-intel-x86_64.tar.gz
cp hmmer-3.1b2-linux-intel-x86_64/binaries/* ../bin/



# EMBOSS Tools 6.x.x
wget -c ftp://emboss.open-bio.org/pub/EMBOSS/MYEMBOSS-6.5.1.tar.gz
tar xvfp MYEMBOSS-6.5.1.tar.gz
pushd MYEMBOSS-6.5.1/
autoconf
./configure
make -j 4
sudo make install
popd



# Augustus so BUSCO does not complain
wget -c http://augustus.gobics.de/binaries/augustus.2.5.5.tar.gz
tar xvfp augustus.2.5.5.tar.gz
pushd augustus.2.5.5/
make -j 8
cp bin/* ../../bin/
popd



# TransDecoder
wget -c https://github.com/TransDecoder/TransDecoder/archive/v2.1.0.tar.gz
tar xzvfp v2.1.0.tar.gz
pushd TransDecoder-2.1.0/
make -j 8
popd



#tRNAScan-SE
wget -c http://lowelab.ucsc.edu/software/tRNAscan-SE.tar.gz
tar -xzvf tRNAscan-SE.tar.gz
pushd tRNAscan-SE-1.3.1/
make -j 8
popd



#ViennaRNA (to use RNAfold)
curl -L "http://www.tbi.univie.ac.at/RNA/download/package=viennarna-src-tbi&flavor=sourcecode&dist=2_2_x&arch=src&version=2.2.4" > ViennaRNA-2.2.4.tar.gz
tar xvf ViennaRNA-2.2.4.tar.gz
pushd ViennaRNA-2.2.4/
./configure PYTHON_VERSION=2.7
make -j 8
cp \
src/bin/{RNAPKplex,RNAsnoop,RNAdistance,RNAplot,RNAheat,RNA2Dfold,RNAaliduplex,RNApdist,RNAinverse,RNALfold,RNAup,RNApaln,RNAduplex,RNAeval,RNALalifold,RNAcofold,RNAparconv,RNAplfold,RNAsubopt,RNAalifold,RNAplex,RNApvmin,RNAfold} \
../../bin/
popd



# Infernal-1.1.1
wget -c http://eddylab.org/infernal/infernal-1.1.1.tar.gz
tar xvf infernal-1.1.1.tar.gz
pushd infernal-1.1.1/
./configure
make -j 8
make check
cp $(find src/ -type f -executable) ../../bin/
popd



# Samtools-1.3
wget -c https://github.com/samtools/samtools/releases/download/1.3/samtools-1.3.tar.bz2
tar xvf  samtools-1.3.tar.bz2
pushd samtools-1.3/
./configure
make -j 8
cp samtools ../../bin/
popd



#QDD-3.1.2
mkdir -p QDD-3.1.2
pushd QDD-3.1.2
wget -c http://net.imbe.fr/~emeglecz/QDDweb/QDD-3.1.2/QDD-3.1.2.tar.gz
tar xvf QDD-3.1.2.tar.gz
popd



# Clustalw-2.1
wget -c ftp://ftp.ebi.ac.uk/pub/software/clustalw2/2.1/clustalw-2.1-linux-x86_64-libcppstatic.tar.gz
tar xvf clustalw-2.1-linux-x86_64-libcppstatic.tar.gz
cp clustalw-2.1-linux-x86_64-libcppstatic/clustalw2 ../bin/



#Primer-3.1
wget -c https://sourceforge.net/projects/primer3/files/primer3/2.3.7/primer3-2.3.7.tar.gz
tar xvf primer3-2.3.7.tar.gz


