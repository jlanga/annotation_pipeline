# Read blast tables
read_blast <- function(filename){
    require(dplyr)
    filename %>%
        read_tsv(
            file = .,
            col_names = c(
                "qseqid", "sseqid", "pident", "length", "mismatch", "gapopen", 
                "qstart", "qend", "sstart", "send", "evalue", "bitscore")
        )
}



# Remove duplicated hits (pairs qseqid-sseqid) using dplyr's criterion
remove_duplicated_hits <- function(blast_table){

    blast_table %>% 
        group_by(qseqid, sseqid) %>%
        filter(row_number() == 1)
}

#From the blast results, take the identifier in which we are interested
get_ids <- function(raw_ids){
    # Convert the gi|whatever1|ref|whatever2| to whatever1
    ids <- rep(0, length(raw_ids)) # Initialize
    for(i in 1:length(raw_ids)){
        # Get an ID
        full_id <- raw_ids[i]
        # Split by |, take the first element in list, and then th
        id <- (str_split(full_id, "\\|") %>% unlist())[2]
        ids[i] <- id
    }
    return(as.integer(ids))
}


# Compute the cogs found
analyze_cog <- function(blast_data_frame){
    blast_data_frame %>%
        mutate(protein_id = get_ids(sseqid)) %>%
        inner_join(x = cog_full, y = ., by = "protein_id")
}

