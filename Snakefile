shell.prefix("set -euo pipefail;")
configfile: "config.yaml"

include: "snakefiles/folders"
include: "snakefiles/software"
include: "snakefiles/raw"
include: "snakefiles/interproscan"
include: "snakefiles/busco"
include: "snakefiles/uniref90"
include: "snakefiles/nr"
include: "snakefiles/cog"
include: "snakefiles/transdecoder"
include: "snakefiles/pfam"
include: "snakefiles/trnas"
include: "snakefiles/mirnas"
include: "snakefiles/ncrnas"
include: "snakefiles/bioservices"
include: "snakefiles/go"
include: "snakefiles/reactome"
include: "snakefiles/qdd"

rule all:
    input:
        uniref       + "uniref90.tsv",
        nr_subset    + "nr_subset.tsv",
        busco        + "vertebrata/full_table",
        busco        + "metazoa/full_table",
        busco        + "eukaryota/full_table",
        cog          + "functions.pdf",  
        ipro         + "ipro.tsv",
        trnas        + "trnascanse.tsv",
        #mirnas       + "blastn_mirbase.tsv",
        mirnas       + "rnafold.tsv",
        ncrnas       + "cmsearch.tsv",
        go           + "go_annotation.tsv",
        reactome     + "reactome_top.pdf",
        pfam         + "pfama.tsv"
        
       
        
rule help: 
    shell:
        """
        echo raw:
            < snakefiles/raw \
            fgrep -h "##" | sed 's/rule /\t/' | sed -e 's/\\$$//' | sed -e 's/##//' | sed 's/\: /:\t/'
        
        echo NR:
            < snakefiles/nr \
            fgrep -h "##" | sed 's/rule /\t/' | sed -e 's/\\$$//' | sed -e 's/##//' | sed 's/\: /:\t/'
        
        echo Uniref90:
            < snakefiles/uniref90 \
            fgrep -h "##" | sed 's/rule /\t/' | sed -e 's/\\$$//' | sed -e 's/##//' | sed 's/\: /:\t/'

        echo Busco \(lineage = arthropoda, eukaryota, metazoa, bacteria, fungi, vertebrata\):
            < snakefiles/busco \
            fgrep -h "##" | sed 's/rule /\t/' | sed -e 's/\\$$//' | sed -e 's/##//' | sed 's/\: /:\t/'
        
        echo COG:
            < snakefiles/cog \
            fgrep -h "##" | sed 's/rule /\t/' | sed -e 's/\\$$//' | sed -e 's/##//' | sed 's/\: /:\t/'
        
        echo TransDecoder:
            < snakefiles/transdecoder \
            fgrep -h "##" | sed 's/rule /\t/' | sed -e 's/\\$$//' | sed -e 's/##//' | sed 's/\: /:\t/'
        
        echo InterProScan:
            < snakefiles/interproscan \
            fgrep -h "##" | sed 's/rule /\t/' | sed -e 's/\\$$//' | sed -e 's/##//' | sed 's/\: /:\t/'
        
        echo miRNAs:
            < snakefiles/mirnas \
            fgrep -h "##" | sed 's/rule /\t/' | sed -e 's/\\$$//' | sed -e 's/##//' | sed 's/\: /:\t/'
        
        echo tRNAs:
            < snakefiles/trnas \
            fgrep -h "##" | sed 's/rule /\t/' | sed -e 's/\\$$//' | sed -e 's/##//' | sed 's/\: /:\t/'
        
        echo ncRNAs:
            < snakefiles/ncrnas \
            fgrep -h "##" | sed 's/rule /\t/' | sed -e 's/\\$$//' | sed -e 's/##//' | sed 's/\: /:\t/'
        """
