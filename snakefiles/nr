rule nr_download: ##data/db/nr.gz - The NR in fasta format
    """
    Download the entire NR fasta from the URL in the config.yaml
    """
    output:
        db + "nr.gz"
    threads:
        1
    params:
        url = config["urls"]["nr"]
    log:
        "logs/nr/download.log"
    benchmark:
        "benchmarks/nr/download.json"
    shell:
        """
        wget \
            --output-file {log} \
            --output-document {output} \
            --continue \
            {params.url}
        """



rule nr_makeblastdb: ##data/db/nr - The Blast database for NR
    """
    Make the NR filtered database
    """
    input:
        db + "nr.gz"
    output:
        touch(db + "nr")
    params:
        taxid = config["taxid"],
    threads:
        1
    log:
        "logs/nr/makeblastdb.log"
    benchmark:
        "benchmarks/nr/makeblastdb.json"
    shell:
        """
        gzip -dc {input} |
        makeblastdb \
            -dbtype prot \
            -title {output} \
            -out {output} \
            -parse_seqids \
        2> {log} 1>&2
        """



rule nr_get_gilist_from_entrez: ## data/nr_subset/gilist.tsv
    """
    Get the set of GIs from the taxa specified in the config.yaml. It will retrieve all the proteins in that and children taxa.
    """
    output:
        tsv = nr_subset + "gilist.tsv"
    threads:
        1
    params:
        url = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=protein&term=txid{}[ORGN]&retmax=100000000".format(config["taxid"])
    log:
        "logs/nr/get_gilist_from_entrez.log"
    benchmark:
        "benchmarks/nr/get_gilist_from_entrez.json"
    shell:
        """
        (wget \
            --output-document - \
            "{params.url}" |
        grep \
            --only-matching \
            --perl-regexp \
            '(?<=<Id>)\w+(?=</Id>)' \
        > {output.tsv} ) \
        2> {log}
        """



rule nr_subset: ##data/db/nr_subset - The subset of the NR database
    """
    Subset the NR database into the ones with GI's from the taxa in file
    """
    input:
        db = db + "nr",
        gi_list = nr_subset + "gilist.tsv"
    output:
        mock = touch(db + "nr_subset"),
        pal = db + "nr_subset.pal",
        gil = db + "nr_subset.p.gil"
    params:
        out = db + "nr_subset"
    threads:
        1
    log:
        "logs/nr/subset.log"
    benchmark:
        "benchmarks/nr/subset.json"
    shell:
        """
        blastdb_aliastool \
            -db {input.db} \
            -dbtype prot \
            -gilist {input.gi_list} \
            -out $(readlink -f {params.out}) \
        2> {log} 1>&2            
        """
    
    
    
rule nr_blastx: ##data/nr/nr_subset.tsv - The Blastx results
    """
    Do the blastx against the NR reduced database.
    """
    input:
        db  = db + "nr_subset",
        fna = sequences + "transcriptome.fa"
    output:
        tsv = nr_subset + "nr_subset.tsv"
    log:
        "logs/nr/blastx.log"
    benchmark:
        "benchmarks/nr/blastx.json"
    threads:
        24
    shell:
        """
        blastx \
            -query {input.fna} \
            -db {input.db} \
            -max_target_seqs 1 \
            -out {output.tsv} \
            -evalue 1e-5 \
            -outfmt 6 \
            -num_threads {threads} \
        2> {log} 1>&2
        """
